package be.intecbrussel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/getsession")
public class LoginServlet extends HttpServlet {

    private final String INPUT = "LoginServlet.input";

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        String input = request.getParameter("sessiontext");
        if (input != null && !input.trim().equals("")){
            HttpSession session = request.getSession();
            synchronized (session){
                session.setAttribute(INPUT, input);
            }
            response.setContentType("text/html");
            response.setCharacterEncoding("UTF-8");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

    }
}

//
//if (input != null && !input.trim().equals("")){
//        HttpSession session = request.getSession();
//        Object o = session.getAttribute(INPUT);
//        session.setAttribute(INPUT,input);
//        }